unit Main;

interface

uses
   Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
   System.Classes, Vcl.Graphics,
   Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls, Vcl.ComCtrls,
   Vcl.ExtCtrls, System.Actions, Vcl.ActnList;

type
   TPages = array of Integer;

   PNode = ^TNode;

   TNode = record
      Parent: PNode;
      ID: Integer;
      Value: string[20];
      Pages: TPages;
      Next: PNode;
      Found: Boolean;
      Subterms: PNode;
   end;

   TMainForm = class(TForm)
      TreeView: TTreeView;
      lblIndex: TLabel;
      mnNode: TPopupMenu;
      mnItemDelete: TMenuItem;
      mnItemSortChildrenAlphabet: TMenuItem;
      mnItemSortChildrenPage: TMenuItem;
      mnItemEdit: TMenuItem;
      mnItemCreateChild: TMenuItem;
      gbNewTerm: TGroupBox;
      edtValue: TLabeledEdit;
      actList: TActionList;
      actCreateTerm: TAction;
      actEditTerm: TAction;
      actSortChildrenAlphabet: TAction;
      actSortChildrenPages: TAction;
      actDeleteTerm: TAction;
      edtPages: TLabeledEdit;
      actUpdateTreeView: TAction;
    GroupBox1: TGroupBox;
    edtEditValue: TLabeledEdit;
    edtEditPages: TLabeledEdit;
    gbSearchTerm: TGroupBox;
    edtSearchTerm: TLabeledEdit;
    gbSearchSubterm: TGroupBox;
    edtSearchSubterm: TLabeledEdit;
    btnSearchTerm: TButton;
    btnSearchSubterm: TButton;
    lbSearch: TListBox;

      procedure FormCreate(Sender: TObject);
      procedure TreeViewContextPopup(Sender: TObject; MousePos: TPoint;
        var Handled: Boolean);
      procedure mnItemCreateChildClick(Sender: TObject);
      procedure mnItemEditClick(Sender: TObject);
      procedure mnItemDeleteClick(Sender: TObject);
      procedure mnItemSortChildrenAlphabetClick(Sender: TObject);
      procedure mnItemSortChildrenPageClick(Sender: TObject);
      procedure actCreateTermExecute(Sender: TObject);

      function GetPagesArray(): TPages;
      function GetPagesArrayEdit(): TPages;
      function AddSubterm(var Parent: PNode): PNode;
      procedure DrawChildren(var Parent: PNode; TreeNode: TTreeNode);
      function FindNodeByID(const ID: Integer; var Node: PNode;
        var Found: Boolean): PNode;
      function FindSubtermByValue(const Str: string; var Node: PNode): PNode;
      function GetSubtermsAmount(const Parent: PNode): Integer;
      procedure RefreshTree();
      procedure AddSorted(var Root: PNode; var Element: Pnode);
      procedure AddSortedByPages(var Root: PNode; var Element: Pnode);

      procedure actUpdateTreeViewExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure actSortChildrenAlphabetExecute(Sender: TObject);
    procedure actEditTermExecute(Sender: TObject);
    procedure actSortChildrenPagesExecute(Sender: TObject);
    procedure actDeleteTermExecute(Sender: TObject);
    procedure btnSearchTermClick(Sender: TObject);
    procedure btnSearchSubtermClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   MainForm: TMainForm;

implementation

{$R *.dfm}

var
   Root: PNode;
   ClickedNode: TTreeNode;
   Total: Integer;

function GetPagesStr(var Node: PNode): string;
var
   i: Integer;
begin
   Result := '( ';
   for i := 0 to Length(Node^.Pages) - 1 do
   begin
      Result := Result + IntToStr(Node^.Pages[i]) + ' ';
   end;
   Result := Result + ')';
end;


procedure TMainForm.actCreateTermExecute(Sender: TObject);
begin
   //
   ShowMessage('asd');
end;

procedure TMainForm.actDeleteTermExecute(Sender: TObject);
begin
   ShowMessage('asd');
end;

procedure TMainForm.actEditTermExecute(Sender: TObject);
begin
   ShowMessage('asd');
end;

function InsertAfterNode(var AfterNode, TargetNode: PNode): PNode;
begin
   TargetNode^.Next := AfterNode^.Next;
   AfterNode^.Next := TargetNode;
end;

procedure TMainForm.AddSorted(var Root, Element: Pnode);
var
   NewNode, Cur: PNode;
begin
   New(NewNode);
   NewNode^.Value := Element^.Value;
   NewNode^.ID := Element^.ID;
   NewNode^.Pages := Element^.Pages;
   NewNode^.Subterms := Element^.Subterms;
   NewNode^.Next := nil;
   NewNode^.Parent := Element^.Parent;
   NewNode^.Found := Element^.Found;


   if Root = nil then
      Root := NewNode
   else
   begin
      Cur := Root;

      // comparison
      if Element^.Value < Cur^.Value then
      begin
         NewNode^.Next := Root;
         Root := NewNode;
      end
      else
      begin

         while Cur^.Next <> nil do
         begin
            if (Element^.Value > Cur^.Value) and (Element^.Value < Cur^.Next^.Value) then
               break;
            Cur := Cur^.Next;
         end;

         InsertAfterNode(Cur, NewNode);
      end;
   end;
end;


// alphabet
procedure TMainForm.actSortChildrenAlphabetExecute(Sender: TObject);
var
   Parent, Temp: PNode;

   SortedRoot: PNode;
   TargetNode, TempRoot: PNode;
   Found: Boolean;
begin
   if ClickedNode = nil then
   begin
      TempRoot := Root;
      Temp := TempRoot;

      New(SortedRoot);
      SortedRoot := nil;

      while Temp <> nil do
      begin
         AddSorted(SortedRoot, Temp);
         Temp := Temp^.Next
      end;

      Found := False;
//      TargetNode := FindNodeByID(TempRoot^.ID, Root, Found);
//      Dispose(Root);

      Root := SortedRoot;
   end
   else
   begin
      TempRoot := PNode(ClickedNode.Data);
      Temp := TempRoot^.Subterms;

      New(SortedRoot);
      SortedRoot := nil;

      while Temp <> nil do
      begin
         AddSorted(SortedRoot, Temp);
         Temp := Temp^.Next
      end;

      Found := False;
      TargetNode := FindNodeByID(TempRoot^.ID, Root, Found);
//      Dispose(TargetNode);

      TargetNode^.Subterms := SortedRoot;
   end;



   RefreshTree();
end;

procedure TMainForm.actSortChildrenPagesExecute(Sender: TObject);
begin
   ShowMessage('asd');
end;

procedure TMainForm.actUpdateTreeViewExecute(Sender: TObject);
begin
   //
end;

function TMainForm.AddSubterm(var Parent: PNode): PNode;
var
   NewNode, Temp, TempRoot, TempSubterm, Nilish: PNode;
begin
   New(NewNode);

   NewNode^.Parent := Parent;
   NewNode^.ID := Total;
   NewNode^.Value := edtValue.Text;
   NewNode^.Pages := GetPagesArray();
   NewNode^.Next := nil;
   NewNode^.Subterms := nil;
   NewNode^.Found := False;

   // can reduce code
   if ClickedNode = nil then
   begin
      Temp := Root;

      if Temp = nil then
         Root := NewNode
      else
      begin
         while Temp^.Next <> nil do
            Temp := Temp^.Next;

         Temp^.Next := NewNode;
      end;
   end

   else
   begin
      Temp := Parent^.Subterms;

      if Temp = nil then
         Parent^.Subterms := NewNode
      else
      begin
         while Temp^.Next <> nil do
            Temp := Temp^.Next;

         Temp^.Next := NewNode;
      end;
   end;

   Nilish := nil;
   Result := Nilish;

   Inc(Total);

   RefreshTree();
   TreeView.FullExpand();
end;


procedure TMainForm.btnSearchSubtermClick(Sender: TObject);
var
   Temp, Cur: PNode;
begin
   lbSearch.Items.Clear;
   FindSubtermByValue(edtSearchSubterm.Text, Root);
end;

procedure TMainForm.btnSearchTermClick(Sender: TObject);
var
   Temp, Child: PNode;
begin
   //
   Temp := Root;
   lbSearch.Clear;

   while Temp <> nil do
   begin
      if Temp^.Value = edtSearchTerm.Text then
         break;
      Temp := Temp^.Next;
   end;

   if Temp = nil then
      lbSearch.Items.Add('Empty :(')
   else
   begin
      Child := Temp^.Subterms;

      lbSearch.Items.Add(Temp^.Value);
      while Child <> nil do
      begin
         lbSearch.Items.Add('- ' + Child^.Value);
         Child := Child^.Next;
      end;
   end;

end;

procedure TMainForm.Button1Click(Sender: TObject);
begin
   // TODO: add sort for root elements
   RefreshTree;
end;

function TMainForm.FindNodeByID(const ID: Integer; var Node: PNode;
  var Found: Boolean): PNode;
var
   Temp: PNode;
begin
   if Node^.ID = ID then
   begin
      Found := True;
      Result := Node;
   end
   else
   begin
      Temp := Node;
      while (Temp <> nil) and not Found do
      begin
         if Temp^.ID = ID then
         begin
            Found := True;
            Result := Temp;
         end
         else
         begin
            if Temp^.Subterms <> nil then
               Result := FindNodeByID(ID, Temp^.Subterms, Found);

            Temp := Temp^.Next;
         end;

      end;
   end;
end;

function TMainForm.FindSubtermByValue(const Str: string; var Node: PNode): PNode;
var
   Temp, Cur: PNode;
begin

   if Node^.Value = Str then
   begin

      lbSearch.Items.Add(Node^.Parent^.Value);
      Cur := Node^.Parent^.Subterms;
      while Cur <> nil do
      begin
         lbSearch.Items.Add('- ' + Cur^.Value);
         Cur := Cur^.Next;
      end;
      Result := Node;
   end
   else
   begin
      Temp := Node;
      while (Temp <> nil) do
      begin
         if (Temp^.Value = Str) then
         begin

            lbSearch.Items.Add(Temp^.Parent^.Value);
            Cur := Temp^.Parent^.Subterms;
            while Cur <> nil do
            begin
               lbSearch.Items.Add('- ' + Cur^.Value);
               Cur := Cur^.Next;
            end;
            Result := Temp;
            Temp := nil;
         end
         else
         begin
            if Temp^.Subterms <> nil then
               Result := FindSubtermByValue(Str, Temp^.Subterms);

            Temp := Temp^.Next;
         end;

      end;

      // last and not found
      if Temp = nil then
         Result := nil;
   end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
   Temp: PNode;
begin
   Root := nil;
   Total := 0;
   Temp := nil;

   edtValue.Text := 'chapter 1';
   edtPages.Text := '2';
   AddSubterm(Temp);

   edtValue.Text := 'chapter 2';
   edtPages.Text := '12';
   AddSubterm(Temp);


   edtValue.Text := 'chapter 3';
   edtPages.Text := '29';
   AddSubterm(Temp);
   RefreshTree();
end;

function TMainForm.GetPagesArray: TPages;
var
   Text, Acc: string;
   i, Total: Integer;

   Pages: TPages;
begin
   Text := edtPages.Text;

   // catch empty pages and title
   SetLength(Pages, 10);
   Total := 0;
   Acc := '';
   for i := 1 to Text.Length do
   begin
      if Text[i] = ' ' then
      begin
         Pages[Total] := StrToInt(Acc);
         Acc := '';
         Inc(Total)
      end
      else
         Acc := Acc + Text[i];
   end;
   Pages[Total] := StrToInt(Acc);
   SetLength(Pages, Total + 1);

   Result := Pages;
end;

function TMainForm.GetPagesArrayEdit: TPages;
var
   Text, Acc: string;
   i, Total: Integer;

   Pages: TPages;
begin
   Text := edtEditPages.Text;

   // catch empty pages and title
   SetLength(Pages, 10);
   Total := 0;
   Acc := '';
   for i := 1 to Text.Length do
   begin
      if Text[i] = ' ' then
      begin
         Pages[Total] := StrToInt(Acc);
         Acc := '';
         Inc(Total)
      end
      else
         Acc := Acc + Text[i];
   end;
   Pages[Total] := StrToInt(Acc);
   SetLength(Pages, Total + 1);

   Result := Pages;
end;


function TMainForm.GetSubtermsAmount(const Parent: PNode): Integer;
var
   Temp: PNode;
   Amount: Integer;
begin
   Amount := 0;
   Temp := Parent^.Subterms;
   while Temp <> nil do
   begin
      Inc(Amount);
      Temp := Temp^.Next;
   end;
   Result := Amount;
end;

procedure TMainForm.mnItemCreateChildClick(Sender: TObject);
var
   Temp: PNode;
   TreeNode: TTreeNode;

   TargetNode: PNode;
   Found: Boolean;
begin
   // !!!
   // attention to node

   Temp := nil;
   if ClickedNode = nil then
      // must add as last sibling
      AddSubterm(Temp)
   else
   begin
      Found := False;
      TargetNode := FindNodeByID(PNode(ClickedNode.Data)^.ID, Root, Found);
      AddSubterm(TargetNode);
   end;

//   RefreshTree();

end;

function DeleteSubterm(var HeadTerm, Element: PNode): PNode;
var
   NewList, Temp: PNode;
begin
//   New(NewList);
//   NewList := nil;

   // if first elem
   if HeadTerm = Element then
   begin
      HeadTerm := Element^.Next;
      Dispose(Element);
   end
   else
   begin
      Temp := HeadTerm;

      while Temp^.Next <> Element do
         Temp := Temp^.Next;

      // here, Temp is prev for Elem
      if Element^.Next <> nil then
         Temp^.Next := Element^.Next
      else
         Temp^.Next := nil;

      Dispose(Element);
   end;

   Result := HeadTerm;
end;

procedure TMainForm.mnItemDeleteClick(Sender: TObject);
var
   TargetNode, TempNode, Parent: PNode;
   Found: Boolean;
begin
   TempNode := PNode(ClickedNode.Data);

   Found := False;
   TargetNode := FindNodeByID(TempNode^.ID, Root, Found);

   if TargetNode^.Parent = nil then
   begin
      DeleteSubterm(Root, TargetNode);
   end
   else
   begin
      Parent := TargetNode^.Parent;
      DeleteSubterm(Parent^.Subterms, TargetNode);
   end;


   RefreshTree();
end;

procedure TMainForm.mnItemEditClick(Sender: TObject);
var
   TargetNode: PNode;
   Found: Boolean;
begin
   Found := False;
   TargetNode := FindNodeByID(PNode(ClickedNode.Data)^.ID, Root, Found);

   TargetNode^.Value := edtEditValue.Text;
   TargetNode^.Pages := GetPagesArrayEdit();

   RefreshTree;
end;

procedure TMainForm.mnItemSortChildrenAlphabetClick(Sender: TObject);
begin
   //
end;

procedure TMainForm.AddSortedByPages(var Root, Element: Pnode);
var
   NewNode, Cur: PNode;
begin
   New(NewNode);
   NewNode^.Value := Element^.Value;
   NewNode^.ID := Element^.ID;
   NewNode^.Pages := Element^.Pages;
   NewNode^.Subterms := Element^.Subterms;
   NewNode^.Next := nil;
   NewNode^.Parent := Element^.Parent;
   NewNode^.Found := Element^.Found;


   if Root = nil then
      Root := NewNode
   else
   begin
      Cur := Root;

      // comparison
      if Element^.Pages[0] < Cur^.Pages[0] then
      begin
         NewNode^.Next := Root;
         Root := NewNode;
      end
      else
      begin

         while Cur^.Next <> nil do
         begin
            if (Element^.Pages[0] > Cur^.Pages[0]) and (Element^.Pages[0] < Cur^.Next^.Pages[0]) then
               break;
            Cur := Cur^.Next;
         end;

         InsertAfterNode(Cur, NewNode);
      end;
   end;
end;

// pages
procedure TMainForm.mnItemSortChildrenPageClick(Sender: TObject);
var
   Parent, Temp: PNode;

   SortedRoot: PNode;
   TargetNode, TempRoot: PNode;
   Found: Boolean;
begin
   if ClickedNode = nil then
   begin
      TempRoot := Root;
      Temp := TempRoot;

      New(SortedRoot);
      SortedRoot := nil;

      while Temp <> nil do
      begin
         AddSortedByPages(SortedRoot, Temp);
         Temp := Temp^.Next
      end;

      Found := False;
//      TargetNode := FindNodeByID(TempRoot^.ID, Root, Found);
//      Dispose(Root);

      Root := SortedRoot;
   end
   else
   begin
      TempRoot := PNode(ClickedNode.Data);
      Temp := TempRoot^.Subterms;

      New(SortedRoot);
      SortedRoot := nil;

      while Temp <> nil do
      begin
         AddSortedByPages(SortedRoot, Temp);
         Temp := Temp^.Next
      end;

      Found := False;
      TargetNode := FindNodeByID(TempRoot^.ID, Root, Found);
//      Dispose(TargetNode);

      TargetNode^.Subterms := SortedRoot;
   end;



   RefreshTree();
end;


procedure TMainForm.DrawChildren(var Parent: PNode; TreeNode: TTreeNode);
var
   Temp: PNode;
   ChildTreeNode: TTreeNode;
begin
   Temp := Parent^.Subterms;

   with TreeView.Items do
   begin
//      ChildTreeNode := nil;
      while Temp <> nil do
      begin
         ChildTreeNode := AddChild(TreeNode, Temp^.Value + ' ' + GetPagesStr(Temp));
         Temp^.Found := False;
         // no coercion here
         ChildTreeNode.Data := Temp;

         DrawChildren(Temp, ChildTreeNode);
         Temp := Temp^.Next
      end;
   end;
end;



procedure TMainForm.RefreshTree;
var
   Temp: PNode;
   TreeNode: TTreeNode;
begin
   Temp := Root;
   with TreeView.Items do
   begin
      Clear;
      TreeNode := nil;
      while Temp <> nil do
      begin
         TreeNode := Add(TreeNode, Temp^.Value + ' ' + GetPagesStr(Temp));
         Temp^.Found := False;
         TreeNode.Data := Temp;

         DrawChildren(Temp, TreeNode);

         Temp := Temp^.Next
      end;

   end;

   TreeView.FullExpand();
end;

procedure TMainForm.TreeViewContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
var
   Point: TPoint;
begin
   GetCursorPos(Point);

   ClickedNode := nil;
   ClickedNode := TreeView.GetNodeAt(MousePos.X, MousePos.Y);

   // if clicked to add root element
   if ClickedNode = nil then
   begin
      actCreateTerm.Enabled := True;
      actSortChildrenAlphabet.Enabled := True;
      actSortChildrenPages.Enabled := True;
      actEditTerm.Enabled := False;
      actDeleteTerm.Enabled := False;
   end
   // if clicked at node
   else
      if GetSubtermsAmount(PNode(ClickedNode.Data)) > 1 then
      begin
         actCreateTerm.Enabled := True;
         actSortChildrenAlphabet.Enabled := True;
         actSortChildrenPages.Enabled := True;
         actEditTerm.Enabled := True;
         actDeleteTerm.Enabled := True;
      end
      else
      begin
         actCreateTerm.Enabled := True;
         actSortChildrenAlphabet.Enabled := False;
         actSortChildrenPages.Enabled := False;
         actEditTerm.Enabled := True;
         actDeleteTerm.Enabled := True;
      end;

   mnNode.Popup(Point.X, Point.Y);
end;

end.
