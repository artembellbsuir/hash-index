object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'MainForm'
  ClientHeight = 483
  ClientWidth = 866
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblIndex: TLabel
    Left = 8
    Top = 13
    Width = 28
    Height = 13
    Caption = 'Index'
  end
  object TreeView: TTreeView
    Left = 8
    Top = 34
    Width = 273
    Height = 272
    Indent = 19
    TabOrder = 0
    OnContextPopup = TreeViewContextPopup
  end
  object gbNewTerm: TGroupBox
    Left = 296
    Top = 34
    Width = 273
    Height = 121
    Caption = 'New term (subterm)'
    TabOrder = 1
    object edtValue: TLabeledEdit
      Left = 11
      Top = 40
      Width = 246
      Height = 21
      EditLabel.Width = 30
      EditLabel.Height = 13
      EditLabel.Caption = 'Value:'
      TabOrder = 0
      Text = 'root'
    end
    object edtPages: TLabeledEdit
      Left = 11
      Top = 88
      Width = 246
      Height = 21
      EditLabel.Width = 129
      EditLabel.Height = 13
      EditLabel.Caption = 'Pages (divided by spaces):'
      TabOrder = 1
      Text = '1 2 3'
    end
  end
  object GroupBox1: TGroupBox
    Left = 296
    Top = 185
    Width = 273
    Height = 121
    Caption = 'Edit term (subterm)'
    TabOrder = 2
    object edtEditValue: TLabeledEdit
      Left = 11
      Top = 40
      Width = 246
      Height = 21
      EditLabel.Width = 30
      EditLabel.Height = 13
      EditLabel.Caption = 'Value:'
      TabOrder = 0
      Text = 'root-new'
    end
    object edtEditPages: TLabeledEdit
      Left = 11
      Top = 88
      Width = 246
      Height = 21
      EditLabel.Width = 129
      EditLabel.Height = 13
      EditLabel.Caption = 'Pages (divided by spaces):'
      TabOrder = 1
      Text = '10 20 30'
    end
  end
  object gbSearchTerm: TGroupBox
    Left = 8
    Top = 329
    Width = 273
    Height = 121
    Caption = 'Search term'
    TabOrder = 3
    object edtSearchTerm: TLabeledEdit
      Left = 11
      Top = 40
      Width = 246
      Height = 21
      EditLabel.Width = 30
      EditLabel.Height = 13
      EditLabel.Caption = 'Value:'
      TabOrder = 0
      Text = 'root-new'
    end
    object btnSearchTerm: TButton
      Left = 11
      Top = 80
      Width = 246
      Height = 25
      Caption = 'Search Term'
      TabOrder = 1
      OnClick = btnSearchTermClick
    end
  end
  object gbSearchSubterm: TGroupBox
    Left = 296
    Top = 329
    Width = 273
    Height = 121
    Caption = 'Search subterm'
    TabOrder = 4
    object edtSearchSubterm: TLabeledEdit
      Left = 11
      Top = 40
      Width = 246
      Height = 21
      EditLabel.Width = 30
      EditLabel.Height = 13
      EditLabel.Caption = 'Value:'
      TabOrder = 0
      Text = 'root-new'
    end
    object btnSearchSubterm: TButton
      Left = 11
      Top = 80
      Width = 246
      Height = 25
      Caption = 'Search Subterm'
      TabOrder = 1
      OnClick = btnSearchSubtermClick
    end
  end
  object lbSearch: TListBox
    Left = 585
    Top = 34
    Width = 273
    Height = 272
    ItemHeight = 13
    TabOrder = 5
  end
  object mnNode: TPopupMenu
    Left = 232
    object mnItemCreateChild: TMenuItem
      Action = actCreateTerm
      OnClick = mnItemCreateChildClick
    end
    object mnItemEdit: TMenuItem
      Action = actEditTerm
      OnClick = mnItemEditClick
    end
    object mnItemDelete: TMenuItem
      Action = actDeleteTerm
      OnClick = mnItemDeleteClick
    end
    object mnItemSortChildrenAlphabet: TMenuItem
      Action = actSortChildrenAlphabet
    end
    object mnItemSortChildrenPage: TMenuItem
      Action = actSortChildrenPages
      OnClick = mnItemSortChildrenPageClick
    end
  end
  object actList: TActionList
    Left = 184
    object actCreateTerm: TAction
      Category = 'Node'
      Caption = 'Create term (subterm)'
      OnExecute = actCreateTermExecute
    end
    object actEditTerm: TAction
      Category = 'Node'
      Caption = 'Edit term (subterm)'
      OnExecute = actEditTermExecute
    end
    object actSortChildrenAlphabet: TAction
      Category = 'Node'
      Caption = 'Sort subterms (by alphabet)'
      OnExecute = actSortChildrenAlphabetExecute
    end
    object actSortChildrenPages: TAction
      Category = 'Node'
      Caption = 'Sort subterms (by pages)'
      OnExecute = actSortChildrenPagesExecute
    end
    object actDeleteTerm: TAction
      Category = 'Node'
      Caption = 'Delete term (subterm)'
      OnExecute = actDeleteTermExecute
    end
    object actUpdateTreeView: TAction
      Category = 'View'
      Caption = 'Update tree'
      OnExecute = actUpdateTreeViewExecute
    end
  end
end
